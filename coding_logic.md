#Coding Logic

Include libraries (Mozzi, oscillators, etc.)

Define oscillator parameters (base frequency, modulation rate, and modulation intensity)

Remap analog input scale to frequency, rate and intensity scales with the Automap function

Assign pin 4 to frequency, pin 12 to intensity and pin 14 to rate

Load digitized cosine waveforms for the carrier frequency and modulation frequency (FM synthesis)

Define FM synthesis parameters (brightness, smoothing, etc.)

setup function:
  Set up the serial output at 115200 baud for debugging purposes
  start Mozzi synth

updateControl function:
  read the capacitive touch sensor pin 4
  map the pin value to carrier frequency
  calculate the modulation frequency
  set the FM oscillator frequencies
  read the capacitive touch sensor pin 12
  map the pin value to FM itensity
  calculate the modulation intensity
  read the capacitive touch sensor pin 14
  map the pin value to FM rate
  calculate the modulation rate
  optional: use serial to print variables for debugging

AudioOutput function:
  modulate carrier frequency with waveform of given intensity and rate
  return audio output

loop function:
  call Mozzi synth and refresh at clock speed