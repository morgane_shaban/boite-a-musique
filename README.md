
# La Boite a Musique
This is the documentation, description and reflections of Morgane and Clément's Fabacademy microchallenge 03 at MDEF 2021 where they developed a prototype of La Boite a Musique.

## What is La Boite a Musique?
La Boite a Musique is a box that makes music by using objects of all weights, size and colours through arranging placing and balancing them to make sounds.

## Plan and Execution of the Project
The overall aim was to create an artifact where players can interact experimentally with object arrangement and sound. The artifact consists of the electronics bed (including the speakers) which lies beneath the capacitive sensor plate and above the moulded and casted objects can be played with. We used the moulding and casting process to try experiment with as many materials, weights and colours to vary the sounds produced when playing with them. We wanted the capacitive sensor to be made up of a grid of multiple pads so the player could manipulate the sounds depending on where they place/move the objects. So we have 6 different copper pads that can be assigned different function (pitch, volume, frequency, modulation). Finally the Boite a Musique is encased and held together with a laser cut plywood box. It acts as a prototype of a tool or game to play and express yourself.


![founds objects](images/found objects.jpg)


## Iteration Process
The entire fabrication process lasted around 2-3 days where we faced several successes and challenges. The design idea started with the aim to make a playful/ interactive meter that would measure someones/ a group of peoples sense of agency. Due to this being subjective we decided that we wanted the process and outcome of the person's/ people's answer to also be subjective. We started by collecting found objects (trash from the street in El Poblenou, Barcelona) to experiment with, arranging and playing with them to understand how they could be used to represent a sense of agency. The Boite a Musique has come quite far from these concepts however it is a prototype that that can still act as a way of expressing yourself. The moulding and casting was an experimental process where we tested different shapes and materials using 3D printed mould (See below for more information). Overall the 3D printed moulds were not as successful as we hoped and many had to be destroyed to retrieve the casted shapes. The electronics process was...

![concept drawing](images/sketches.jpg)

## What materials do you need?
Electronics
- speakers (8 Ohms, 0.25 watt, 50mm diameter) x 2
- precision potentiometer x 2
- ESP32 feather microcontroller
- PAM8403 amplifier chip
- breadboard
- jumper wires/ F-M dupont wires

Capacitive Sensor
- 150 x 100mm copper tape
- 300 x 300mm bubble wrap
- 300 x 300mm aluminium foil sheet

Plywood case
- 4.2mm plywood (approx. 650mm x 650mm sheet)

Moulding and Casting
- PLA filament (3D printed moulds)
- colour dyes
- release agent (polytek, pol-ease 2500)
- Acrylcast, mineral casting system (40:100 liquid:powder)
- EasyL 940, silicone casting (1:1 A:B)

Machines
- 3D Printer (Ender, Creality)
- Laser cutter (Speedy 400, Trotec)

![digital models](images/digital files.jpg)
![casting](images/casting.jpg)
![laser cutting](images/laser cutting.jpg)

## Step by Step _ fabrication of the moulds and casts:
3D PRINTED MOULDS
Instructions: Draw desired shape (we used the shapes drawn by children from Morgane's intervention/ project) at desired size, (for example approx 90 x 90 mm sized shapes.) Then extrude the shapes with 1.2mm walls, 1.5mm base, (maybe increase width of walls and base if you would like more durable moulds.) Save as a stl file and send to 3D printer

SILICONE CASTING
- 3D printed mould
- colour dyes
- release agent (polytek, pol-ease 2500)
- EasyL 940, silicone casting (1:1 A:B)
Instructions: Start by filling mould with water to understand approximate volume needed to cast. Then proceed to prep the mould by using appropriate release agent. Start measuring amount need of part A into mixing pot then measure and pour in part B, mix with a spatula (follow silicone instructions for ratio of parts eg. 1:1 A:B for EasyL 940). Add desired colour and continue mixing. Once mixed well pour into mould carefully. Tap to release bubbles. Then leave to cure for appropriate amount (e.g 6h for EasyL 940). Release cast after this time. You are read to play!

MINERAL/ACRYLIC CASTING
- mould (3D PRINTED MOULD NOT IDEAL FOR THIS CASTING)
- colour dyes
- Acrylcast, mineral casting system (40:100 liquid:powder)
Instructions: Start by filling mould with water to understand approximate volume needed to cast. Then proceed to prep the mould by using appropriate lightly sprinkling some Acrylcast power on all sides. Start measuring amount need of the liquid into mixing pot then measure and pour in the powder, let it sit for a minute then mix with a spatula (follow mineral casting instructions for ratio of parts eg. 40:100 liquid:powder for Acrylcast). Add desired colour and continue mixing. Once mixed well and not lumps pour into mould carefully. Tap to release bubbles. Then leave to cure for appropriate amount (e.g 6-12 mins for Acrylcast). Release cast after this time. You are read to play!

Tips:
- Remember when mould is hard (e.g. PLA 3D printed mould) always cast with soft/flexible materials (e.g. silicone)
- when making a 1 part mould tapering shapes can help release them after cast is cured

## Step by Step _ fabrication of the laser cut box:
Laser cutter (Speedy 400, Trotec) Power - 60, Speed - 0.5, Frequency - 1000Hz

Tips:
- Remember to consider and test tolerances for pressfit design

![testing](images/testing.jpg)
![assembling](images/assembling.jpg)


## Electronics

For this project, we wanted to control the synthesizer in an unconventional way: a way that nobody had experienced to make music before. The theremin (https://en.wikipedia.org/wiki/Theremin) was a major inspiration for us: it is the only instrument that is played without touching it. The theremin also relies of capacitive sensing to vary the pitch and volume of the oscillators: the hands of the player act as the grounded plate of the capacitor in a LC circuit oscillator. Whereas the theremin is fully analog, our boite à musique is digital: it only relies on capacitive sensing as the input to our digital FM synth, Mozzi. 

We fabricated a DIY capacitive sensor array with adhesive copper film: we cut 6 rectangular pads which we stuck onto a wood plate, and soldered to wires connected to an ESP32 microcontroller. The dielectric layer of our DIY capacitor was simply made with bubble wrap. We experimented with neoprene foam, but we needed a more compliant material to increase the dynamic range of our sensor. Instead of relying on the human player to act as the grounded plate, we used a grounded sheet of aluminium foil. Indeed, the boite à musique relies on the weight, shape and position of objects on the top surface tto vary the distance between the grounded top layer and each capacitive pad.

## Coding Logic

Include libraries (Mozzi library on GitHub https://github.com/sensorium/Mozzi.git)

Define oscillator parameters (base frequency, modulation rate, and modulation intensity)

Remap analog input scale to frequency, rate and intensity scales with the Automap function

Assign pin 4 to frequency, pin 12 to intensity and pin 14 to rate

Load digitized cosine waveforms for the carrier frequency and modulation frequency (FM synthesis)

Define FM synthesis parameters (brightness, smoothing, etc.)

setup function{
Set up the serial output at 115200 baud for debugging purposes
start Mozzi synth
}

updateControl function{
read the capacitive touch sensor pin 4
map the pin value to carrier frequency
calculate the modulation frequency
set the FM oscillator frequencies
read the capacitive touch sensor pin 12
map the pin value to FM itensity
calculate the modulation intensity
read the capacitive touch sensor pin 14
map the pin value to FM rate
calculate the modulation rate
optional: use serial to print variables for debugging
}

AudioOutput function{
modulate carrier frequency with waveform of given intensity and rate
return audio output
}

loop function{
call Mozzi synth and refresh at clock speed
}
![play](images/play.jpg)

## Future Developments
La Boite a Musique acts as a prototype, with much potential. First we can experiment further with different shapes, materials, and sounds, (perfecting the casting and moulding, whilst experimenting further and debugging the electronics). There is the possibility of scaling it up and making it into an installation project where you move human scaled objects and play with them and your body to make sculptures and music. (maybe using the found objects/trash we started with).

## What you will find in this repo
- 3D Rhino model files, of the box and the moulds
- Laser cut files, of the box
- 3D printing files, of the moulds
- Electronic Schematics
- Electronic arduino files
- Images of the process

## Downloads
-  

## Links to personal Posts
_Morgane Sha'ban : https://morgane_shaban.gitlab.io/mdef/challenge03.html
_Clement Rames : https://clement_rames.gitlab.io/mdef-website/
